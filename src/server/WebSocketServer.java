package server;

/**
 * io File used to read the file
 * net URL used to load jar file
 * net Socket used to create sockets
 * util HashMap and HashSet used to store web application objects and 
 * routing, application related configuration
 * util Properties used to read server configuration
 * util concurrent Executors used for multi-thread management
 * util Logger used for logging
 * application WebSocketApp is used to load server applications
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import application.WebSocketApp;

/**
 * @(#)WebSocketServer.java
 * 
 * Created: 2014/02/15
 * Last modified: 2014/02/17 Due
 * Date: 2014/02/18 Copyright (c) 2014 Rovshen Nazarov.
 * 
 * All rights reserved.
 * Baylor University. Spring 2014. Server for Advanced Data Communication class
 * Simple multi-threaded Web Socket Server implementation.
 * @author Rovshen Nazarov
 * @version 1.1
 */
public class WebSocketServer implements Runnable {
    
    /** for logging */
    protected static volatile Logger        LOG              = Logger.getLogger(WebSocketServer.class
                                                                     .getName());
    /** map of applications and classes necessary to load them */
    protected final HashMap<String, String> appsNames        = new HashMap<String, String>();
    /** server routing table */
    protected final HashMap<String, String> routingTable     = new HashMap<String, String>();
    /** set of loaded application objects */
    protected final HashSet<WebSocketApp>   loadedApps       = new HashSet<WebSocketApp>();
    /** threads for better control over connection threads */
    protected ArrayList<Thread>             threadsArr       = new ArrayList<Thread>();
    /** the port that the server will be listening on */
    private final int                       portNumber;
    /** list of connected sockets to this serer */
    private final HashSet<Socket>           connectedSockets = new HashSet<Socket>();
    /** server status, so that we can stop server dispatch */
    private volatile boolean                serverRunningStatus;
    /** server thread for starting server dispatch loop */
    private Thread                          serverThread;
    /** server socket */
    private ServerSocket                    server;
    
    /**
     * Constructor for the WebSocketServer.
     * 
     * @param portNumber
     * @param LIMIT_OF_CONNECTED_CLIENTS
     * @param serverPath
     * to limit number of connection at the same time
     * @throws IOException
     * if an I/O error occurs when opening the socket.
     * @throws IllegalArgumentException
     * if the port parameter is outside the specified range of valid
     * port values
     */
    public WebSocketServer(int portNumber)
            throws IOException, IllegalArgumentException {
    
        this.portNumber = portNumber;
        try {
            /* A server socket is opened with a connectivity queue of a size
             * specified in int */
            server = new ServerSocket(this.portNumber);
            serverRunningStatus = true;
            LOG.log(Level.INFO, "Bound to the port " + server.getLocalPort());
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Cannot open port " + this.portNumber);
            System.exit(1);
        }
    }
    
    /**
     * Generate threads pull, load server routing configurations and server
     * applications and start the dispatch loop.
     */
    @Override
    public void run() {
    
        loadConfiguration();
        loadApps();
        startServer();
    }
    
    /**
     * Stop server gracefully and terminate all server related threads and
     * sockets on error or from terminal
     * 
     * @throws IOException
     */
    public void stopServer() throws IOException {
    
        LOG.log(Level.INFO, " stopping server.");
        for (Thread currThr : threadsArr) {
            try {
                /** wait for thread in millisec, 1 sec */
                int waitTime = 1000;
                currThr.join(waitTime);
            } catch (InterruptedException e) {
                LOG.log(Level.FINER,
                        " client thread could not be stopped. "
                                + e.getMessage());
            }
        }
        for (Socket currSock : connectedSockets) {
            currSock.close();
        }
        server.close();
        serverRunningStatus = false;
    }
    
    /**
     * Server main, takes as input the port number to listen on
     * 
     * @param args
     * port and path to the server folder
     * @throws IOException
     */
    public static void main(String[] args) {
    
        int predefinePort = 0;
        if (args.length == 1) {
            try {
                predefinePort = Integer.parseInt(args[0]);
            } catch (NullPointerException | IndexOutOfBoundsException
                    | NumberFormatException e) {
                System.out.println("Provide port number (1st argument)");
                LOG.log(Level.SEVERE,
                        "Error occured while trying to read provided arguments");
                /* stop the server */
                System.exit(1);
            }
        } else {
            /* inform user of the server usage */
            System.out.println("Provide port number (1st argument)");
            LOG.log(Level.FINER, "Provide port number (1st argument).");
            /* stop the server */
            System.exit(1);
        }
        
        WebSocketServer listenServerSocket = null;
        try {
            /** create server socket with predefined socket queue limit */
            listenServerSocket = new WebSocketServer(predefinePort);
        } catch (IOException | IllegalArgumentException exp) {
            LOG.log(Level.SEVERE, "Cannot bind to the port " + predefinePort
                    + exp.toString());
            System.exit(-1);
        }
        /** create server thread */
        listenServerSocket.serverThread = new Thread(listenServerSocket,
                "WebSocketServer");
        /** start server thread */
        listenServerSocket.serverThread.start();
        LOG.log(Level.INFO, "Server started at " + predefinePort);
    }// end of main
    
    /**
     * Read text files in the provided path. Split the lines into key value
     * pairs. If the file is not found throw IOException. If the file name is
     * null throw NullPointerException
     * 
     * @param filePath
     * @return HashMap of key value pairs of type String
     * @throws IOException
     * @throws NullPointerException
     */
    protected HashMap<String, String> readTextFile(String filePath)
            throws IOException, NullPointerException, IllegalArgumentException {
    
        if (filePath == null) { throw new NullPointerException(); }
        /** use properties to read data from the configuaration file */
        Properties prop = new Properties();
        File file = new File(filePath);
        FileInputStream readFile = new FileInputStream(file);
        /** load the file data into properties */
        prop.load(readFile);
        readFile.close();
        HashMap<String, String> resultMap = new HashMap<String, String>();
        
        /** get the properties and put them in the map */
        for (Object key : prop.keySet()) {
            String value = prop.get(key).toString();
            resultMap.put(key.toString(), value);
        }
        /** check if the constracted map is empty */
        if (resultMap.isEmpty()) { throw new IllegalArgumentException(); }
        return resultMap;
    }
    
    /**
     * Load all registered applications.
     */
    private void loadConfiguration() {
    
        /** Absolute paths to server configurations */
        String FILE_APPS = "apps.conf";
        String FILE_ROUTING = "routing.conf";
        try {
            /* Examined apps.conf to get the list of registered applications.
             * Examined the routing.conf to get the routing information */
            appsNames.putAll(readTextFile(FILE_APPS));
            routingTable.putAll(readTextFile(FILE_ROUTING));
            /** log the apps in the server routing table */
            for (String appURL : routingTable.keySet()) {
                LOG.log(Level.INFO, " Route to " + routingTable.get(appURL)
                        + " application exists in the server routing table.");
            }
        } catch (NullPointerException | IOException
                | ArrayIndexOutOfBoundsException | IllegalArgumentException exp) {
            LOG.log(Level.SEVERE,
                    "Could not open apps and routing config files "
                            + exp.toString() + " Stopping the server thread "
                            + serverThread.getName());
            /** a nonzero status code indicates abnormal termination. */
            int status = 1;
            System.exit(status);
        } catch (Exception exp) {
            LOG.log(Level.SEVERE,
                    "Could not open apps and routing config files "
                            + exp.toString() + " Stopping the server thread "
                            + serverThread.getName());
            /** a nonzero status code indicates abnormal termination. */
            int status = 1;
            System.exit(status);
        }
        
        /* If there is an unknown application in the routing.conf, print the
         * error to the LOG and terminate. */
        for (String appURI : routingTable.keySet()) {
            if (appsNames.get(routingTable.get(appURI)) != null) {
            } else {
                LOG.log(Level.SEVERE,
                        "Unexisting application "
                                + routingTable.get(appURI)
                                + "  for the route "
                                + appURI
                                + " in routing.config. Stopping the server thread "
                                + serverThread.getName());
                /** a nonzero status code indicates abnormal termination. */
                System.exit(1);
            }
        }// end loading routes
    }
    
    /**
     * Start the server dispatch loop
     */
    private void startServer() {
    
        /** run until the server is terminated */
        while (serverRunningStatus && !serverThread.isInterrupted()) {
            
            /** get next client connection */
            Socket clientSocketConnection = null;
            try {
                clientSocketConnection = server.accept();
                LOG.log(Level.INFO, "Client connected: "
                        + clientSocketConnection.getRemoteSocketAddress()
                                .toString());
                /** add the client socket to the set of sockets */
                connectedSockets.add(clientSocketConnection);
                /** start a separate thread for the newly connected client */
                /** create a thread pull with limited threads */
                Thread clientThread = new Thread(handleConnnection(
                        clientSocketConnection,
                        loadedApps, appsNames, routingTable, LOG));
                clientThread.start();
                threadsArr.add(clientThread);
                
            } catch (IOException | NullPointerException exp) {
                LOG.log(Level.SEVERE, "Error accepting client connection "
                        + exp.toString());
                exp.printStackTrace();
                
                try {
                    stopServer();
                } catch (IOException | NullPointerException e) {
                    LOG.log(Level.SEVERE,
                            "Error while stopping server prematurily "
                                    + e.toString());
                    e.printStackTrace();
                }
            }
        }// end of server dispatch loop
        try {
            stopServer();
        } catch (IOException | NullPointerException e) {
            LOG.log(Level.SEVERE,
                    "Error while stopping server prematurily "
                            + e.toString());
        }
    }// end of server start
    
    /**
     * 
     */
    private void loadApps() {
    
        /**
         * Start all the applications by instantiating the class specified in
         * apps.conf using Java Reflection.
         */
        for (String appName : appsNames.keySet()) {
            /* If the sub-folder or jar of any registered application is
             * missing, or if the class cannot be instantiated, server will
             * record in Log the error details and terminate. */
            URL jarfile = null;
            try {
                /** load the apps from the apps folder in the server directory */
                File file = new File("apps/" + appName + "/" + appName + ".jar");
                if (file.getName().endsWith(".jar")) {
                    /** create URLs based on the file path */
                    jarfile = file.toURI().toURL();
                    /** prepare the class loader to load the class */
                    ClassLoader loader = URLClassLoader.newInstance(
                            new URL[] { jarfile }, getClass().getClassLoader());
                    
                    /** get the class for the given class name */
                    Class<?> dynamiclyLoadedClass = Class.forName(
                            appsNames.get(appName), true, loader);
                    /* generate a new instance of the application class
                     * dynamically using Java reflection */
                    WebSocketApp loadedApp = (WebSocketApp) dynamiclyLoadedClass
                            .newInstance();
                    /* add the generated application object to the set of the
                     * sever apps */
                    loadedApps.add(loadedApp);
                } else {
                    LOG.log(Level.SEVERE,
                            "Cannot find " + appsNames.get(appName)
                                    + " jar file.");
                    System.exit(1);
                }
            } catch (NullPointerException | ClassNotFoundException
                    | IllegalAccessException | MalformedURLException
                    | InstantiationException exp) {
                LOG.log(Level.SEVERE,
                        "Cannot initiate " + appsNames.get(appName)
                                + " application. From URL: " + jarfile
                                + "  Exp:" + exp.toString());
                System.exit(1);
            }
        }// end loading application classes
    }
    
    /**
     * A helper class to generate a new connection handler class with provided
     * parameters for the connection.
     * 
     * @param clientSocket
     * @param lOG_IN
     * @return WebSocketConnectionHandler instance
     * @throws IOException
     */
    private WebSocketConnectionHandler handleConnnection(Socket clientSocket,
            HashSet<WebSocketApp> apps, HashMap<String, String> appsNames,
            HashMap<String, String> routingTable, Logger lOG_IN)
            throws IOException {
    
        return new WebSocketConnectionHandler(clientSocket, apps, appsNames,
                routingTable, lOG_IN);
        
    }
    
}
