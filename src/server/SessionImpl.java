package server;

/**
 * net Socket used to manage client socket
 * IO streams used to get/send client socket data
 * util HashMap and Set are used to store message handler
 * util Logger used for logging
 */
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.WebSocketFrame;
import model.WebSocketFrame.Opcode;
import application.MessageHandler;
import application.Session;
import application.WebSocketApp;
import exception.BadValueException;

/**
 * @(#)SessionImpl.java
 * 
 * Created: 2014/02/15
 * Last modified: 2014/02/17
 * Due Date: 2014/02/18
 * Copyright (c) 2014 Rovshen Nazarov.
 * All rights reserved. Baylor University. Spring 2014.
 * Implementation of the {@link Session} for Advanced Data Communication class.
 * Session implementation for the Web Socket Server application interaction.
 * 
 * @author Rovshen Nazarov
 * @version 1.1
 * 
 * Baylor University. Spring 2014. Server for Advanced Data Communication class.
 * A session is an object provided by the server
 * to identify a connection. Session is created by the server when new
 * WebSocket connection is established. Server passes session to
 * application using {@link WebSocketApp#onConnect(application.Session)} method.
 * Since
 * then session is considered to be open until an invalid packet is
 * received or connection is closed by application ({@link #close()} method), by
 * client ( {@link WebSocketApp#onClose(application.Session)} method) or due to
 * an error (
 * {@link WebSocketApp#onError(application.Session, java.lang.Throwable)}
 * method).
 * @author Rov Nazarov extended from interface {@link Session} by Team Right .
 */
public class SessionImpl implements Session {
    
    /** A set of {@link MessageHandler} associated with this session */
    protected final Set<MessageHandler> msgHandlerSet = new HashSet<MessageHandler>();
    /** Communicate with the client using the returned Socket’s OutputStream. */
    protected final OutputStream        clientOutStr;
    
    /** for logging messages */
    private static volatile Logger      LOG;
    /** status of this session */
    private boolean                     openStatus;
    /** client socket associated with this session */
    private final Socket                clientSocket;
    
    /**
     * SessionImp constructor. Constructs an instance of the {@link SessionImpl}
     * 
     * @param clientSocketIn
     * @throws IOException
     */
    public SessionImpl(Socket clientSocketIn, Logger LOG_IN) throws IOException {
    
        /* get reference to the server logger */
        SessionImpl.LOG = LOG_IN;
        if (clientSocketIn != null) {
            openStatus = true;
            clientSocket = clientSocketIn;
            clientOutStr = clientSocketIn.getOutputStream();
        } else {
            throw new NullPointerException();
        }
    }
    
    /**
     * Registers the given message handler. This handler will be informed about
     * all messages that are received by this session. All previously registered
     * handlers will be still receiving all the messages.
     * 
     * @param handler
     * Handler to register.
     * @throws NullPointerException
     * When {@code handler} is null.
     * @throws IllegalStateException
     * When session is closed.
     */
    @Override
    public void addMessageHandler(MessageHandler msgHandler)
            throws NullPointerException, IllegalStateException {
    
        if (msgHandler == null) {
            throw new NullPointerException();
        }
        if (!this.isOpen()) {
            throw new IllegalStateException();
        }
        synchronized (msgHandlerSet) {
            msgHandlerSet.add(msgHandler);
        }
    }
    
    /**
     * Closes the given session and the underlying socket connection.
     * 
     * @throws IOException
     * When failed to close the socket connection.
     * @throws IllegalStateException
     * When session is closed.
     */
    @Override
    public void close() throws IOException, IllegalStateException {
    
        if (!isOpen()) {
            throw new IllegalStateException();
        } else {
            /* change session state to false and close the client socket.
             * Closing socket also closes I/O streams */
            openStatus = false;
            if (!clientSocket.isClosed()) {
                clientSocket.close();
            }
            
            if (!clientSocket.isClosed()) {
                throw new IOException("Session could not close client socket.");
            }
        }
    }
    
    /**
     * This method returns whether the connection is open. Connection is
     * considered to be open unless {@link close} method was called by the
     * application or one of {@link WebSocketApp#onClose(application.Session) }
     * and
     * {@link WebSocketApp#onError(application.Session, java.lang.Throwable)
     * }
     * was called by the server with this session as a parameter.
     * 
     * @return If the session is open.
     */
    @Override
    public boolean isOpen() {
    
        return openStatus;
    }
    
    /**
     * This method sends message to the given connection using
     * {@link SessionImpl#sendMsgHelper(int, boolean, byte[])} .
     * 
     * @param msg
     * Message to sent. Cannot be {@code null}.
     * @throws IOException
     * This exception is thrown when there is an error during
     * serialization or during transmission.
     * @throws NullPointerException
     * When {@code msg} is null.
     * @throws IllegalStateException
     * When session is closed.
     */
    @Override
    public void sendMessage(String msg) throws IOException {
    
        if (msg == null) {
            throw new NullPointerException("The message is null");
        }
        if (!isOpen()) {
            throw new IllegalStateException(
                    "The connection was closed, when writing to output stream.");
        }
        
        /** opcode, is fin, payload */
        try {
            sendMsgHelper(1, true, msg.getBytes());
        } catch (BadValueException exp) {
            LOG.log(Level.SEVERE, " problems with sending the message " + msg
                    + exp.toString());
        }
    }
    
    /**
     * This method sends message to the given connection using
     * {@link SessionImpl#sendMsgHelper(int, boolean, byte[])} .
     * 
     * @param msg
     * Message to sent. Cannot be {@code null}.
     * @throws IOException
     * This exception is thrown when there is an error during
     * serialization or during transmission.
     * @throws NullPointerException
     * When {@code msg} is null.
     * @throws IllegalStateException
     * When session is closed.
     */
    @Override
    public void sendMessage(byte[] msg) throws IOException {
    
        if (msg == null) {
            throw new NullPointerException("The message is null");
        }
        if (!isOpen()) {
            throw new IllegalStateException(
                    "The connection was closed, when writing to output stream.");
        }
        
        /** opcode, is fin, payload */
        try {
            sendMsgHelper(1, false, msg);
        } catch (BadValueException exp) {
            LOG.log(Level.SEVERE, " problems with sending the message "
                    + new String(msg) + exp.toString());
        }
    }
    
    /**
     * Helper function to send the data to the client using OutputStream
     * 
     * @param opcode
     * @param isFin
     * @param payloadIn
     * @throws BadValueException
     * @throws IOException
     */
    private void sendMsgHelper(int opcode, boolean isFin, byte[] payloadIn)
            throws BadValueException, IOException {
    
        WebSocketFrame webSocketFrameToSend = null;
        /* create a new web socket frame and initialize it */
        webSocketFrameToSend = new WebSocketFrame(isFin,
                Opcode.getOpcode(opcode), payloadIn);
        
        /** send the data to the client */
        if (!clientSocket.isOutputShutdown()) {
            /* send the web socket frame throw the client socket's output stream */
            webSocketFrameToSend.encode(clientOutStr);
            clientOutStr.flush();
        }
        
    }
    
}
