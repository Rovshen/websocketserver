package server;

/**
 * io BufferedReader to read from the client socket input stream
 * io InputStreamReader to read from the client socket input stream
 * net Socket used to operate client socket
 * security.MessageDigest used for {@link WebSocketConnectionHandler#serverKey(String clientKey)}
 * util HashMap and HashSet used to store web application objects and 
 * routing, application related configuration
 * util Logger used for logging
 *  application WebSocketApp is used to operate server applications
 *  model.WebSocketFrame used to get and send web socket frames to and from client
 *  model.WebSocketFrame.Opcode used to check received frames at {@link WebSocketConnectionHandler#run()}
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.WebSocketFrame;
import model.WebSocketFrame.Opcode;
import application.MessageHandler;
import application.WebSocketApp;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import exception.BadValueException;

/**
 * @(#)SessionImpl.java
 * 
 * Created: 2014/02/15
 * Last modified: 2014/02/17
 * Due Date: 2014/02/18
 * Copyright (c) 2014 Rovshen Nazarov.
 * All rights reserved. Baylor University. Spring 2014.
 * 
 * Baylor University. Spring 2014. Server for Advanced Data Communication class
 * A helper class for accepting and manipulating each client's socket.
 * 
 * @author Rovshen Nazarov
 * @version 1.1
 * 
 */
public class WebSocketConnectionHandler implements Runnable {
    
    /** connection logger */
    protected static volatile Logger        LOG;
    /** map of applications and classes necessary to load them */
    protected final HashMap<String, String> appsNames                 = new HashMap<String, String>();
    /** server routing table for client GET request's routing */
    protected final HashMap<String, String> routingTable              = new HashMap<String, String>();
    protected final HashMap<String, String> clientSocketReqHeaderData = new HashMap<String, String>();
    /** client output stream */
    protected final OutputStream            clientOutStr;
    /** client input stream */
    protected final InputStream             clientInStr;
    
    /** GUUI used to calculate the key returned to client */
    private final static String             GUID                      = new String(
                                                                              "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
    /* client socket */
    private final Socket                    clientSocket;
    /** server application requested by client */
    private WebSocketApp                    webSocketApp;
    /** all server applications supported by the server */
    private final HashSet<WebSocketApp>     webSocketApps             = new HashSet<WebSocketApp>();
    
    /**
     * WebSocketConnectionHandler Constructor with parameters
     * 
     * @param clientSocket
     * @param apps
     * server application objects
     * @param appsNames
     * @param routingTable
     * @param LOG
     * @throws IOException
     * thrown if IOException on sockets encountered
     */
    public WebSocketConnectionHandler(Socket clientSocket,
            HashSet<WebSocketApp> apps, HashMap<String, String> appsNames,
            HashMap<String, String> routingTable, Logger LOG)
            throws IOException {
    
        /* get reference to the server logger */
        WebSocketConnectionHandler.LOG = LOG;
        
        /** check that necessary input values are not null */
        if (clientSocket != null && apps != null && appsNames != null
                && routingTable != null) {
            
            /** assign values to the current connection object */
            this.clientSocket = clientSocket;
            /** get the app that should handle this request */
            webSocketApps.addAll(apps);
            this.appsNames.putAll(appsNames);
            this.routingTable.putAll(routingTable);
            clientOutStr = clientSocket.getOutputStream();
            clientInStr = clientSocket.getInputStream();
        } else {
            throw new NullPointerException();
        }
    }
    
    /**
     * Main method of the connection. This method performs web socket handshake
     * {@link WebSocketConnectionHandler#performOpeningHandShakeFromSocket()}
     * Then the request server application is loaded into the connection using
     * routing table {@link WebSocketConnectionHandler#routeSocketToApp(String)}
     * The a new session created with the client socket for connection between
     * server and server application Then the main while loop started where the
     * connection reads client socket input until the socket is closed and
     * Performs required actions based on {@link Opcode}
     */
    @Override
    public void run() {
    
        SessionImpl sess = null;
        
        try {
            performOpeningHandShakeFromSocket();
            /** create new session for this connection */
            sess = new SessionImpl(clientSocket, LOG);
            webSocketApp = routeSocketToApp(clientSocketReqHeaderData
                    .get("GET"));
            /** call {@link WebSocketApp#onConnect(Session sess)} */
            synchronized (webSocketApp) {
                webSocketApp.onConnect(sess);
            }
            
            WebSocketFrame webFrameReceived, webSocketFrameToSend;
            
            /** read from input stream until the client closes the connection */
            while (!clientSocket.isInputShutdown() && !clientSocket.isClosed()) {
                boolean expMasked = true;
                webFrameReceived = new WebSocketFrame(clientInStr, expMasked);
                /** if received PING reply with PONG */
                if (webFrameReceived.getOpcode().getValue() == Opcode.PING
                        .getValue()) {
                    /** is fin, opcode, payload */
                    webSocketFrameToSend = new WebSocketFrame(true,
                            Opcode.PONG, webFrameReceived.getPayload());
                    webSocketFrameToSend.encode(clientOutStr);
                }
                /** if received CLOSE_CONNECTION close connection */
                if (webFrameReceived.getOpcode().getValue() == Opcode.CLOSE_CONNECTION
                        .getValue()) {
                    /** call application onClose() */
                    synchronized (webSocketApp) {
                        webSocketApp.onClose(sess);
                    }
                    /* send the reply close frame, close the connection */
                    webSocketFrameToSend = new WebSocketFrame(true,
                            Opcode.CLOSE_CONNECTION, new byte[] {});
                    webSocketFrameToSend.encode(clientOutStr);
                    /* session may already be closed */
                    if (sess.isOpen()) {
                        sess.close();
                    }
                }
                /** We agreed not to support CONTINUATION_FRAME */
                
                /** if received BINARY_FRAME call onMessage */
                if (webFrameReceived.getOpcode().getValue() == Opcode.BINARY_FRAME
                        .getValue()) {
                    synchronized (sess) {
                        /**
                         * invoke all {@link MessageHandler#onMessage(byte[])}
                         * for the given session
                         */
                        for (MessageHandler msgHandl : sess.msgHandlerSet) {
                            msgHandl.onMessage(webFrameReceived.getPayload());
                        }
                    }
                }
                /** if received TEXT_FRAME call onMessage */
                if (webFrameReceived.getOpcode().getValue() == Opcode.TEXT_FRAME
                        .getValue()) {
                    synchronized (sess) {
                        /**
                         * invoke all {@link MessageHandler#onMessage(String)}
                         * for the given session
                         */
                        String tempStr;
                        for (MessageHandler msgHandl : sess.msgHandlerSet) {
                            tempStr = new String(webFrameReceived.getPayload());
                            try {
                                /* check the text encoding, expected UTF-8 */
                                tempStr.getBytes("UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                /* fail the connection RFC 6455 8.1 */
                                throw new IOException(
                                        "The text format is incorrect");
                            }
                            
                            msgHandl.onMessage(tempStr);
                        }
                    }
                }
            }// end of socket input output loop
            
        } catch (IOException | BadValueException | NullPointerException
                | NoSuchAlgorithmException e) {
            try {
                LOG.log(Level.FINE,
                        " problems with the connection, closing prematurly. "
                                + e.toString());
                webSocketApp.onError(sess, e);
                /* session may already be closed, if the server application
                 * closed the session before */
                if (sess.isOpen()) {
                    sess.close();
                }
            } catch (IOException | NullPointerException exp) {
                try {
                    if (sess != null) {
                        sess.close();
                    }
                } catch (IllegalStateException | IOException expOut) {
                    LOG.log(Level.SEVERE,
                            " could not close the session. "
                                    + expOut.toString());
                }
                LOG.log(Level.SEVERE,
                        " problems during error closing. Closing the session. "
                                + exp.toString());
            }
        }
    }// end of the run
    
    /**
     * Helper method for routing. Routes based on Get and available server
     * applications.
     * 
     * @param appURLIn
     * @return requested WebSocketApp instance
     * @throws NullPointerException
     * if could not find such application
     */
    private WebSocketApp routeSocketToApp(String appURLIn) {
    
        String appClassName = null;
        /** check all routes in the table */
        for (String appURL : routingTable.keySet()) {
            /* check if the requested application URL exists in the routing
             * table */
            if (appURL.equals(appURLIn)) {
                appClassName = appsNames.get(routingTable.get(appURL));
            }
        }
        /* if the requested server app exists check if the corresponding server
         * app instance was loaded on the server */
        if (appClassName != null) {
            for (WebSocketApp webApp : webSocketApps) {
                String appName = webApp.getClass().toString().split("@")[0]
                        .split(" ")[1];
                /** if the server app is loaded return it */
                if (appClassName.equals(appName)) { return webApp; }
            }
        } else {
            LOG.log(Level.SEVERE, " Could not route request: " + appURLIn);
            throw new NullPointerException();
        }
        return null;
    }
    
    /**
     * Helper method for opening handshake. Build the server response using
     * stored client request headers
     * {@link WebSocketConnectionHandler#clientSocketReqHeaderData} as specified
     * in RFC 6455.
     * 
     * @throws IOException
     * @throws BadValueException
     * @throws NullPointerException
     * @throws NoSuchAlgorithmException
     */
    private void performOpeningHandShakeFromSocket() throws IOException,
            BadValueException, NullPointerException, NoSuchAlgorithmException {
    
        /** parse the client HTTP request */
        parseClientRequest();
        
        /* check if the client request is web socket request as specified in RFC
         * 6455 */
        try {
            if (!clientSocketReqHeaderData.get("Upgrade").equals("websocket")
                    || !clientSocketReqHeaderData.get("Sec-WebSocket-Version")
                            .equals("13")
                    || !clientSocketReqHeaderData.get("Connection").contains(
                            "Upgrade")) {
                LOG.log(Level.SEVERE, "The protocol RFC 6455 not followed");
                throw new IOException();
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "The protocol RFC 6455 not followed");
            throw new IOException();
        }
        
        /** Get the server accept key */
        String serverReplyKey = serverKey(clientSocketReqHeaderData
                .get("Sec-WebSocket-Key"));
        
        final String[] replyHeader = new String[5];
        /** prepare server reply */
        if (!clientSocket.isOutputShutdown()) {
            replyHeader[0] = "HTTP/1.1 101 Switching Protocols\r\n";
            replyHeader[1] = "Upgrade: websocket\r\n";
            replyHeader[2] = "Connection: Upgrade\r\n";
            replyHeader[3] = "Sec-WebSocket-Accept: " + serverReplyKey + "\r\n";
            /** end of reply headers */
            replyHeader[4] = "\r\n";
        }
        /** send server reply to the client using client socket output stream */
        for (int i = 0; i < replyHeader.length; i++) {
            clientOutStr.write(replyHeader[i].getBytes("UTF-8"));
        }
        
        clientOutStr.flush();
    }// end performOpeningHandShakeFromSocket
    
    /**
     * Helper function to read the client request and store request headers.
     * 
     * @throws IOException
     */
    private void parseClientRequest() throws IOException {
    
        if (!clientSocket.isInputShutdown()) {
            
            String[] tempArr;
            /** use BufferedReader to read one line at a time */
            BufferedReader readInputStream = new BufferedReader(
                    new InputStreamReader(clientInStr));
            String requestLineURL;
            /** check if the request is HTTP/1.1 */
            if ((requestLineURL = readInputStream.readLine()) != "") {
                String[] reqUrlArr = requestLineURL.split(" ");
                if (!reqUrlArr[2].equals("HTTP/1.1")) {
                    LOG.log(Level.FINE, "The protocol " + reqUrlArr[2]
                            + " is not supported");
                    throw new IOException();
                }
                clientSocketReqHeaderData.put(reqUrlArr[0], reqUrlArr[1]);
            } else {
                LOG.log(Level.FINE, "The header format is incorrect "
                        + requestLineURL);
                throw new IOException();
            }
            /** read the next line of the header */
            String requestLine = readInputStream.readLine();
            
            /* read client header until the empty line, which is the end of the
             * header */
            while (!requestLine.equals("")) {
                /** remove trailing spaces */
                requestLine = requestLine.replace(" ", "");
                /** split the header line into 2 */
                tempArr = requestLine.split(":", 2);
                
                if (tempArr.length == 2) {
                    /** store the header values into the map */
                    clientSocketReqHeaderData.put(tempArr[0], tempArr[1]);
                } else {
                    LOG.log(Level.FINE, "The header format is incorrect "
                            + requestLine);
                    throw new IOException();
                }
                /** read next header line */
                requestLine = readInputStream.readLine();
            }// end while loop
            
        }// end if loop
        
        LOG.log(Level.INFO, "url " + clientSocketReqHeaderData.get("GET"));
    }// end parseClientRequest
    
    /**
     * Get server key based on client key with
     * {@link WebSocketConnectionHandler#GUID}, digest it with SHA-1 and encode
     * it in base64 as defined in RFC 6455.
     * 
     * @param clientKey
     * received from client request header
     * @return String server accept key
     * @throws NoSuchAlgorithmException
     * if SHA-1 is not available
     */
    private String serverKey(String clientKey) throws NoSuchAlgorithmException {
    
        /** add required magic GUID as stated in RFC 6455 */
        String keyReply = clientKey + GUID;
        /** get SHA-1 algorithm */
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        /** load the key into the SHA-1 */
        md.update(keyReply.getBytes(), 0, keyReply.length());
        /** produce SHA-1 digest message */
        byte[] hashedKey = md.digest();
        /** encode the hashedKey with base64 */
        final String serverReplyKey = new String(Base64.encode(hashedKey));
        return serverReplyKey;
    }
}
