apps/
This folder must contain subfolder for every registered application. The sub-folder might contain any files important to the application. Only one file is mandatory - the application jar. The subfolder name and the jar name must exactly match the string key of the application.

