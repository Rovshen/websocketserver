package tests;

import java.util.Random;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import client.WebSocketClientMulticlientChat;

/**
 * User connects to the server, sends a text paragraph, waits a second and
 * repeats sending a message 50 times. After it sends all messages, the user
 * closes connection
 * 
 * @author Roman Smetana
 */
public class SendMessagesTest extends AbstractJavaSamplerClient {
	/**
	 * set up default arguments for the JMeter GUI
	 */
	@Override
	public Arguments getDefaultParameters() {
		Arguments defaultParameters = new Arguments();
		defaultParameters.addArgument("hostname", "localhost");
		defaultParameters.addArgument("port", "11111");
		defaultParameters.addArgument("appURL", "/chat5321");
		return defaultParameters;
	}

	/**
	 * Run the test
	 */
	@Override
	public SampleResult runTest(JavaSamplerContext context) {
		SampleResult result = new SampleResult();
		result.sampleStart(); // start stopwatch

		try {
			WebSocketClientMulticlientChat client = new WebSocketClientMulticlientChat(context.getParameter("hostname"), context.getIntParameter("port"), context.getParameter("appURL"));
			Random r = new Random();
			String[] msgs = new String[] {
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vehicula, elit non sodales interdum, elit tellus rhoncus augue, in facilisis tellus sapien ac ante. Nulla varius nibh pharetra nunc accumsan, sed dictum dolor mattis. Integer tincidunt elit sit amet elit hendrerit interdum. Donec vitae lacus sodales, congue lorem ut, laoreet est. Nulla vitae magna faucibus, mollis urna a, consequat lorem. Cras in elementum turpis. Integer rutrum, mi non sollicitudin sodales, orci velit vestibulum velit, ut pulvinar augue tortor et neque. Suspendisse gravida purus elit, sit amet mattis magna egestas sed. Ut non massa id est congue varius sit amet id elit. Proin vel ligula ipsum. In quam erat, imperdiet sed lobortis sed, iaculis sit amet eros. Integer neque neque, dignissim vel facilisis at, sagittis eget nulla.",
					"Phasellus fringilla eros sem, sed pharetra diam vulputate eget. Fusce faucibus lorem quis lorem placerat, non blandit odio commodo. Aenean tincidunt elit vel nulla pellentesque viverra. Vestibulum elementum viverra quam, sed adipiscing tortor laoreet ut. Phasellus eu libero ac sapien varius viverra. Nullam non magna interdum, tincidunt nisl ac, ultricies lacus. Proin et lobortis enim. Phasellus sed velit ultrices ligula vulputate mattis vel sed urna. Mauris eget pretium mi. Maecenas commodo magna velit, non ultricies nunc rhoncus eget. Duis fringilla euismod pellentesque. Aliquam bibendum ipsum non neque pharetra, ut pharetra nisi facilisis. Donec iaculis enim orci, at sagittis turpis blandit vitae.",
					"Aliquam augue odio, congue id nulla quis, sollicitudin scelerisque purus. Phasellus ut magna porta, feugiat ligula non, mollis elit. Proin sit amet enim feugiat, porttitor ipsum eu, elementum libero. Donec id ultricies lacus. Nunc non ultrices mi, id tincidunt risus. Aenean et nisl sed justo pellentesque rutrum at ut sapien. Sed hendrerit enim vulputate venenatis accumsan. Phasellus tortor lorem, commodo in dignissim vel, blandit vel lorem. Duis volutpat, odio non euismod tempus, nisl augue sagittis velit, tempus volutpat purus felis eu arcu. Nullam facilisis enim ut augue commodo ornare. Praesent dapibus lacus sit amet varius bibendum. Vestibulum adipiscing nulla nec lectus rhoncus dictum. Donec a fermentum odio. Cras a risus vitae magna suscipit viverra at auctor purus.",
					"Praesent non risus scelerisque odio scelerisque facilisis. Vivamus accumsan vehicula magna quis pulvinar. Integer mattis ipsum sed dui convallis accumsan. Sed scelerisque diam ante, eu dictum massa suscipit nec. Ut nibh dolor, porta et est cursus, convallis rhoncus neque. Phasellus pretium nulla id molestie consequat. In hac habitasse platea dictumst. Nullam vitae eros sed orci tempor sagittis. Duis accumsan aliquet lacus in semper.",
					"Donec porttitor lobortis leo. Maecenas porttitor, quam eu commodo cursus, ligula elit pellentesque est, sit amet ornare purus nisi ac arcu. Praesent cursus lorem in enim facilisis dignissim. Morbi nec magna eget odio rutrum rhoncus. Cras imperdiet vehicula dolor eget egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non odio vulputate, tincidunt quam eget, consectetur ipsum." };
			for (int i = 0; i < 50; i++) {
				client.sendMessage(msgs[r.nextInt(msgs.length)]);
				Thread.sleep(1000);
			}
			client.close();
			result.sampleEnd(); // stop stopwatch
			result.setSuccessful(true);
			result.setResponseMessage("Successfully performed action");
			result.setResponseCodeOK(); // 200 code
		} catch (Exception e) {
			e.printStackTrace();
			result.sampleEnd(); // stop stopwatch
			result.setSuccessful(false);
			result.setResponseMessage("Exception: " + e);
			result.setResponseCode("500");
		}
		return result;
	}
}
